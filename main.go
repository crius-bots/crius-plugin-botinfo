package crius_plugin_botinfo

import (
	"context"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	cc "gitlab.com/RPGPN/criuscommander/v2"
)

var PluginInfo = &cc.PluginJSON{
	Name:               "BotInfo",
	License:            "BSD-3-Clause",
	Creator:            "Ben <Ponkey364>",
	URL:                "https://gitlab.com/RPGPN/crius-plugin-botinfo",
	Description:        "General bot related commands",
	SupportedPlatforms: cc.PlatformDiscord | cc.PlatformTwitch | cc.PlatformGlimesh,
	Commands: []*cc.PJCommand{
		{
			HandlerName:        "source",
			Name:               "Source",
			Help:               "Provides a link to the bot's source code",
			Activator:          "source",
			SupportedPlatforms: cc.PlatformDiscord | cc.PlatformTwitch | cc.PlatformGlimesh,
		},
		{
			HandlerName:        "changelog",
			Name:               "Changelog",
			Help:               "Provides a link to the bot's changelog",
			Activator:          "changelog",
			SupportedPlatforms: cc.PlatformDiscord | cc.PlatformTwitch | cc.PlatformGlimesh,
		},
	},
}

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	return map[string]cc.CommandHandler{
		"source":    Source,
		"changelog": Changelog,
	}, nil
}

func Source(m *cc.MessageContext, _ []string) error {
	url := CriusUtils.GetOtherConfig(m.Context)["URL"].(string)
	m.Send("You can find my code at " + url)
	return nil
}

func Changelog(m *cc.MessageContext, _ []string) error {
	url := CriusUtils.GetOtherConfig(m.Context)["changelogs"].(string)
	m.Send("You can find my changelog at " + url)
	return nil
}
